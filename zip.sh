#!/bin/bash

zip -r other-window.xpi LICENSE README.md\
    manifest.json web icons target src\
    package.json package-lock.json tsconfig.json\
&& cp other-window.xpi other-window.zip
