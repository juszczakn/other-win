import { otherWinUtil, FfWindow, Tab, SortedWindow } from "./otherWinUtil.js";
import { OptionRuntimeMessage } from "./options/options.js";

const menuContexts: browser.contextMenus.ContextType[] = ["link", "tab"];

browser.contextMenus.onClicked.addListener(function (onClickData, tab) {
  otherWinUtil.getAllWindows().then((data) => {
    const isUrlLinkClick = !!onClickData.linkUrl;
    const isTabClick = !isUrlLinkClick;

    // if they just clicked on the main option (no window id), just pick the first
    // non-focused window.
    let otherWindowId = data.filter((win) => {
      return !win.focused;
    })[0].id;

    const menuItemId: string = onClickData.menuItemId as string;

    let possibleWindowId = menuItemId.split("-");
    if (possibleWindowId.length === 3) {
      otherWindowId = parseInt(possibleWindowId[2]);
    }

    const otherWindow: FfWindow = data.filter((win) => {
      return win.id == otherWindowId;
    })[0];

    const tabLength: number = otherWindow?.tabs?.length || 0; // can't be undefined

    if (isUrlLinkClick) {
      openLinkInOtherWindow(
        onClickData.linkUrl as string,
        tabLength,
        otherWindowId as number,
      );
    } else if (isTabClick) {
      otherWinUtil.moveTab(otherWindow, tab as Tab, () => null);
    }
  });
});

function openLinkInOtherWindow(
  linkUrl: string,
  tabLength: number,
  otherWindowId: number,
) {
  browser.tabs
    .create({
      active: true,
      index: tabLength,
      url: linkUrl,
      windowId: otherWindowId,
    })
    .then(() => {
      otherWinUtil.getOptions().then((opts) => {
        if (opts.contextMenuClickBringsFocus) {
          browser.windows.update(otherWindowId, { focused: true });
        }
      });
    });
}

async function createMenuItem({
  name,
  window,
  position,
}: {
  name: string;
  window: FfWindow;
  position: number;
}) {
  let windowName = await otherWinUtil.getWindowLabel(name, window, position);

  browser.contextMenus.create({
    id: "other-win-" + window.id,
    title: "Send to " + windowName,
    type: "normal",
    contexts: menuContexts,
  });
}

// Just create a menu item without giving window numbers
function createOpenLinkInFirstWindow() {
  browser.contextMenus.create({
    id: "other-win", // no window id, dynamically choose
    title: "Send to other window",
    type: "normal",
    contexts: menuContexts,
  });
}

function createAllContextMenus() {
  browser.contextMenus.removeAll();

  otherWinUtil.getAllWindows().then((windows) => {
    if (windows.length == 1) {
      return;
    } else if (windows.length == 2) {
      createOpenLinkInFirstWindow();
    } else {
      otherWinUtil.sortWindows(windows).then((wins: SortedWindow[]) => {
        wins.forEach((win) => {
          createMenuItem(win);
        });
      });
    }
  });
}

function setWindowTitlePrefix({ name, window, position }: SortedWindow) {
  const windowName = name || "Window " + position;
  const prefix = windowName + " - ";
  browser.windows.update(window.id as number, { titlePreface: prefix });
}

function clearWindowTitlePrefixes() {
  otherWinUtil.getAllWindows().then((allWindows) => {
    allWindows.forEach((window) => {
      browser.windows.update(window.id as number, { titlePreface: "" });
    });
  });
}

function setWindowTitlePrefixes() {
  otherWinUtil.getOptions().then((opts) => {
    if (!opts.addWindowTitlePrefix) {
      return;
    }
    otherWinUtil.getAllWindows().then((allWindows) => {
      otherWinUtil.sortWindows(allWindows).then((windows) => {
        windows.forEach((windowVals) => {
          setWindowTitlePrefix(windowVals);
        });
      });
    });
  });
}

function createInitialContextMenus() {
  createAllContextMenus();
  // just recreate the whole gosh darn thing every time
  browser.windows.onCreated.addListener(createAllContextMenus);
  browser.windows.onRemoved.addListener(createAllContextMenus);
  // active tab changed
  browser.tabs.onActivated.addListener(createAllContextMenus);
}

function recreateInitialContextMenus() {
  otherWinUtil.getOptions().then((opts) => {
    browser.contextMenus.removeAll();
    browser.windows.onCreated.removeListener(createAllContextMenus);
    browser.windows.onRemoved.removeListener(createAllContextMenus);
    browser.tabs.onActivated.addListener(createAllContextMenus);
    if (!opts.hideContextMenu) {
      createInitialContextMenus();
    }
  });
}

otherWinUtil.getOptions().then((res) => {
  if (res.hideContextMenu) {
    // we're done here.
    return;
  }

  createInitialContextMenus();
});

// Options menu changed
browser.runtime.onMessage.addListener((request: OptionRuntimeMessage) => {
  if (request.recreateWindowSettings) {
    recreateInitialContextMenus();
    setWindowTitlePrefixes();
  } else if (request.clearAllWindowNames) {
    otherWinUtil.clearAllWindowNames();
    setWindowTitlePrefixes();
  } else if (request.clearWindowPrefixes) {
    clearWindowTitlePrefixes();
  }
});

browser.windows.onCreated.addListener(setWindowTitlePrefixes);
browser.windows.onRemoved.addListener(setWindowTitlePrefixes);

// Run once on startup to set all window titles.
setWindowTitlePrefixes();
