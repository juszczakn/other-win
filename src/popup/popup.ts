import {
  otherWinUtil,
  FfWindow,
  Tab,
  OtherWindowsObj,
} from "../otherWinUtil.js";

// enum
const mousebutton = {
  left: 1,
  middle: 2,
  right: 3,
};

function closePopup() {
  let ul = document.getElementById("otherWindows")!;
  ul.innerHTML = "";
  window.close();
}

/**
 * Create the input element for setting the current windows name, and add the hooks
 * for saving the name change on [enter].
 */
function customizeCurrentWindow(
  currentWindow: FfWindow,
  currentWindowPosition: number,
) {
  let nameMeText = document.createElement("input");
  nameMeText.placeholder = "Custom Window " + currentWindowPosition + " Name";

  // if this window already has a name then give it that
  otherWinUtil.getWindowName(currentWindow).then((res) => {
    if (res) {
      nameMeText.value = res;
    }
  });

  nameMeText.onkeyup = function (evt) {
    if (evt.keyCode === 13) {
      if (!nameMeText.value) {
        return;
      }
      otherWinUtil
        .setWindowName(currentWindow, nameMeText.value)
        .then((res) => {
          browser.runtime
            .sendMessage({ recreateWindowSettings: true })
            .then(() => {
              closePopup();
            });
        });
    }
  };

  let nameMe = document.createElement("li");
  nameMe.setAttribute("class", "no-hover");

  let gear = document.createElement("img");
  gear.setAttribute("src", "icons/gear-128.png");
  gear.setAttribute("class", "gear");

  gear.onclick = () => {
    browser.runtime.openOptionsPage();
    closePopup();
  };

  nameMe.appendChild(nameMeText);
  nameMe.appendChild(gear);

  return nameMe;
}

/*
 * Left click: move tab, focus new window
 * Middle Click: move tab, no focus change
 * Right Click: do not move tab, focus new window
 */
function performClickAction(
  otherWindow: FfWindow,
  currentTab: Tab,
  btnClicked: number,
) {
  otherWinUtil.getOptions().then((res) => {
    const swapLeftMiddleClick = res.swapLeftMiddleClick;
    if (swapLeftMiddleClick) {
      if (mousebutton.left === btnClicked) {
        btnClicked = mousebutton.middle;
      } else if (mousebutton.middle === btnClicked) {
        btnClicked = mousebutton.left;
      }
    }

    // on right click, just move to that window
    if (mousebutton.right === btnClicked) {
      browser.windows.update(otherWindow.id!, { focused: true });
      closePopup();
      return;
    }
    // else move the window
    otherWinUtil
      .moveTab(otherWindow, currentTab, () => null)
      .finally(() => {
        // workaround: for some reason, with 3+ windows, popup moving tab kills promise chain,
        // and it's not able to update the moved tab to be active in the new window.
        // Manually updating it again in a finally block here seems to work for some reason.
        browser.tabs.update(currentTab.id!, { active: true }).then(() => {
          if (mousebutton.left === btnClicked) {
            browser.windows.update(otherWindow.id!, { focused: true });
          } else {
            closePopup();
          }
        });
      });
  });
}

/**
 * Create window list element
 */
function createWindowListElt(otherWindow: FfWindow, currentTab: Tab) {
  let li = document.createElement("li");
  // don't open the right click menu
  li.addEventListener("contextmenu", (event) => event.preventDefault());

  li.onmouseup = (evt) => {
    performClickAction(otherWindow, currentTab, evt.which);
  };
  return li;
}

function createWindowListElts(
  currentWindow: FfWindow,
  currentTab: Tab,
  orderedWindows: Array<{ name: string; window: FfWindow; position: number }>,
) {
  // display which window to choose
  let ul = document.getElementById("otherWindows")!;

  let currentWindowPosition = 0;
  // ensure that our li's are inserted in the right order by chaining promises together
  let p: Promise<void> = Promise.resolve();
  for (let i = 0; i < orderedWindows.length; i++) {
    let { name, window, position } = orderedWindows[i];

    // if we're on the current window just skip it
    if (currentWindow.id === window.id) {
      currentWindowPosition = position;
      continue;
    }

    let li = createWindowListElt(window, currentTab);

    p = p.then(async (_) => {
      li.style.textAlign = "left";
      li.textContent = await otherWinUtil.getWindowLabel(
        name,
        window,
        position,
      );
      ul.appendChild(li);
    });
  }

  p.then((_) => {
    let customizeTextBox = customizeCurrentWindow(
      currentWindow,
      currentWindowPosition,
    );
    ul.appendChild(customizeTextBox);
  }).then((_) => {
    document.querySelector("html")?.setAttribute("style", "display: block;");
  });
}

function moveWindowOrCreatePopup(data: OtherWindowsObj) {
  let otherWindows = data.otherWindows;

  if (otherWindows.length === 0) {
    // you need at least two windows bub.
    closePopup();
    return;
  }

  document.querySelector("html")?.setAttribute("style", "display: none;");
  // Two choices, if there are only two windows, just switch.
  // otherwise, show the popup to let the user choose.
  if (otherWindows.length === 1) {
    // just switch windows to the other
    performClickAction(otherWindows[0], data.currentTab, mousebutton.left);
  } else {
    let allWindows = otherWindows.slice();
    allWindows.push(data.currentWindow);
    // just order the windows by their ids, hopefully that's order created
    let orderedWindowsPromise = otherWinUtil.sortWindows(allWindows);

    orderedWindowsPromise.then((orderedWindows) => {
      createWindowListElts(data.currentWindow, data.currentTab, orderedWindows);
    });
  }
}

otherWinUtil.getOtherWindows(moveWindowOrCreatePopup);
