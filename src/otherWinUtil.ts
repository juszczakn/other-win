export type Tab = browser.tabs.Tab;
export type FfWindow = browser.windows.Window;

export type GlobalOptions = {
  hideContextMenu: boolean;
  swapLeftMiddleClick: boolean;
  contextMenuClickBringsFocus: boolean;
  addWindowTitlePrefix: boolean;
};

export const windowNameKey = "windowName";

export type OtherWindowsObj = {
  otherWindows: FfWindow[];
  currentWindow: FfWindow;
  currentTab: Tab;
};

export type SortedWindow = { name: string; window: FfWindow; position: number };

class OtherWindowUtil {
  moveTab(otherWindow: FfWindow, currentTab: Tab, callback: () => void) {
    const moveProps = {
      windowId: otherWindow.id,
      index: -1,
    };

    const moveTheTab = () => {
      browser.tabs
        .move(currentTab.id!, moveProps)
        .then((movedTab) => {
          let tab: Tab;
          if (movedTab instanceof Array) {
            tab = movedTab[0];
          } else {
            tab = movedTab as Tab;
          }
          return browser.tabs.update(tab.id!, { active: true });
        })
        .then((tab) => {
          if (callback) {
            return callback();
          }
        });
    };

    return this.getAllWindows()
      .then((windows) => {
        // issue #15; If this is the only tab in the window, keep the window open.
        const window = windows.filter((win) => {
          return currentTab.windowId === win.id;
        })[0];
        if (window?.tabs?.length === 1) {
          return browser.tabs.create({});
        }
        return Promise.resolve(null);
      })
      .then(moveTheTab);
  }

  getAllWindows() {
    return browser.windows.getAll({
      windowTypes: ["normal"],
      populate: true,
    });
  }

  getOtherWindows(
    windowsRetrievedCallback: (windowObj: OtherWindowsObj) => void,
  ) {
    const windowsP = this.getAllWindows();

    windowsP.then((windows) => {
      let otherWindows = windows.filter((win) => {
        return !win.focused;
      });

      // there has to be a current window and tab...
      const currWindow: FfWindow = windows.filter((win) => {
        return win.focused;
      })[0];

      const currentTab = currWindow?.tabs?.filter((tab) => {
        return tab.active;
      })[0]!!;

      windowsRetrievedCallback({
        otherWindows: otherWindows,
        currentWindow: currWindow,
        currentTab: currentTab!!,
      });
    });
  }

  getCurrentTab() {
    return this.getAllWindows().then((windows) => {
      return windows
        ?.filter((win) => {
          return win.focused;
        })[0]
        ?.tabs?.filter((tab) => {
          return tab.active;
        })[0];
    });
  }

  getWindowName(window: FfWindow): Promise<string> {
    return browser.sessions.getWindowValue(
      window.id as number,
      windowNameKey,
    ) as Promise<string>;
  }

  setWindowName(window: FfWindow, windowNameValue: string) {
    return browser.sessions.setWindowValue(
      window.id as number,
      windowNameKey,
      windowNameValue,
    );
  }

  clearAllWindowNames() {
    browser.windows
      .getAll({
        windowTypes: ["normal"],
        populate: true,
      })
      .then((windows) => {
        windows.map((window) => {
          browser.sessions.removeWindowValue(
            window.id as number,
            windowNameKey,
          );
        });
      });
  }

  /**
   * Sort windows in a set order, named windows first (alphanumeric), then unnamed windows
   * in the order in which they were created.
   * returns a promise that resolves to the array of {name, window, position}, sorted by name
   *   alphabetically, then by position
   */
  sortWindows(windows: FfWindow[]): Promise<SortedWindow[]> {
    let windowsSortedById: { position: number; window: FfWindow }[] = windows
      .sort((x: FfWindow, y: FfWindow) => (x.id || 0) - (y.id || 0))
      .map((win, idx) => {
        return {
          position: idx + 1,
          window: win,
        };
      });

    let promises = windowsSortedById.map((win) => {
      return this.getWindowName(win.window).then((name) => {
        // if it has a name, prefix zero so it sorts before unnamed windows.
        // Slice is for leftpad.
        return {
          name: name,
          // ensure ones with names come before ones without
          sortVal: name ? " " + name : ("00000" + win.window.id).slice(-5),
          window: win.window,
          position: win.position,
        };
      });
    });

    return Promise.all(promises).then((values) => {
      return values.sort((x, y) => {
        // sort alphanumerically
        return x.sortVal.localeCompare(y.sortVal);
      });
    });
  }

  getOptions(): Promise<GlobalOptions> {
    return browser.storage.local.get({
      hideContextMenu: false,
      swapLeftMiddleClick: false,
      contextMenuClickBringsFocus: false,
      addWindowTitlePrefix: false,
    }) as Promise<GlobalOptions>;
  }

  async getWindowLabel(name: string, window: FfWindow, position: number) {
    const maxLength = 40;
    const tabs = await browser.tabs.query({ windowId: window.id });
    const activeTab = (
      await browser.tabs.query({ windowId: window.id, active: true })
    )[0];
    var winNameTabs =
      tabs.length == 0 ? "Empty Window" : `"${activeTab.title}"`;
    if (winNameTabs.length > maxLength) {
      winNameTabs = winNameTabs.substring(0, maxLength) + "…";
    }
    return (
      (name || `Window ${position}`) +
      ` : [${tabs.length} tab${tabs.length > 1 ? "s" : " "}] ${winNameTabs}${
        tabs.length > 1 ? ", etc." : ""
      }`
    );
  }
}

export const otherWinUtil = new OtherWindowUtil();
