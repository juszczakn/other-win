import { otherWinUtil } from "../otherWinUtil.js";

export type OptionRuntimeMessage = {
  recreateWindowSettings: boolean | undefined;
  clearWindowPrefixes: boolean | undefined;
  clearAllWindowNames: boolean | undefined;
};

function getChecked(id: string) {
  const input = document?.querySelector("#" + id) as HTMLInputElement;
  return input.checked;
}

function setChecked(id: string, val: boolean) {
  const input = document.querySelector("#" + id) as HTMLInputElement;
  input.checked = val;
}

function saveOptions() {
  otherWinUtil
    .getOptions()
    .then((opts) => {
      const addPrefix = getChecked("addWindowTitlePrefix");
      browser.storage.local.set({
        hideContextMenu: getChecked("hideContextMenu"),
        swapLeftMiddleClick: getChecked("swapLeftMiddleClick"),
        contextMenuClickBringsFocus: getChecked("contextMenuClickBringsFocus"),
        addWindowTitlePrefix: addPrefix,
      });

      const addPrefixChanged = addPrefix !== opts.addWindowTitlePrefix;
      if (addPrefixChanged && !addPrefix) {
        browser.runtime.sendMessage({ clearWindowPrefixes: true });
      }
    })
    .then((_) => {
      browser.runtime.sendMessage({ recreateWindowSettings: true });
    });
}

function restoreOptions() {
  otherWinUtil.getOptions().then((res) => {
    setChecked("hideContextMenu", res.hideContextMenu);
    setChecked("swapLeftMiddleClick", res.swapLeftMiddleClick);
    setChecked("contextMenuClickBringsFocus", res.contextMenuClickBringsFocus);
    setChecked("addWindowTitlePrefix", res.addWindowTitlePrefix);
  });
}

document
  ?.getElementById("clearAllWindowNames")
  ?.addEventListener("click", () => {
    browser.runtime.sendMessage({ clearAllWindowNames: true });
  });

document?.addEventListener("DOMContentLoaded", restoreOptions);
document?.querySelector("form")?.addEventListener("change", saveOptions);
